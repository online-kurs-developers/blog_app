POSTGRES_HOST=postgres
POSTGRES_PORT=5432
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_DATABASE=blog_db

-include .env.docker
  
DB_URL="postgresql://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@$(POSTGRES_HOST):$(POSTGRES_PORT)/$(POSTGRES_DATABASE)?sslmode=disable"
# DB_URL="postgresql://postgres:postgres@52.91.185.142:5432/blog_db?sslmode=disable"
print:
	echo "$(DB_URL)"
	
swag-init:
	swag init -g api/api.go -o api/docs

start:
	go run cmd/main.go

migrateup:
	migrate -path migrations -database "$(DB_URL)" -verbose up

migrateup1:
	migrate -path migrations -database "$(DB_URL)" -verbose up 1

migratedown:
	migrate -path migrations -database "$(DB_URL)" -verbose down

migratedown1:
	migrate -path migrations -database "$(DB_URL)" -verbose down 1

local-up:
	docker-compose --env-file ./.env.docker up -d

.PHONY: start migrateup migratedown
up:
 	docker-compose --env-file ./.env.docker up -d

down:
 	docker-compose down